package top.wecreate.snet;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class SnetConnectionPool {

	private static SnetConnectionPool instance;

	private ExecutorService threadPool;

	private SnetConnectionPool() {
		threadPool = Executors.newCachedThreadPool();
	}

	public static SnetConnectionPool getInstance() {
		if (instance == null) {
			synchronized (SnetConnectionPool.class) {
				if (instance == null) {
					instance = new SnetConnectionPool();
				}
			}
		}
		return instance;
	}

	public void excute(Runnable runnable) {
		threadPool.execute(runnable);
	}
}
