package top.wecreate.snet;

import java.util.List;
import java.util.Map;

public class TestResponse {
	private int code;
	private Map<String, List<String>> header;
	private byte[] body;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Map<String, List<String>> getHeader() {
		return header;
	}

	public void setHeader(Map<String, List<String>> header) {
		this.header = header;
	}

	public byte[] getBody() {
		return body;
	}

	public void setBody(byte[] body) {
		this.body = body;
	}
}
