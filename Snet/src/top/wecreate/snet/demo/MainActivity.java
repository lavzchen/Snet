package top.wecreate.snet.demo;

import java.util.HashMap;
import java.util.Map;

import top.wecreate.snet.R;
import top.wecreate.snet.SnetBaseConnection.ConnetionBuilder;
import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		//1. 构建请求所需的参数，比如url，参数之类的
		ConnetionBuilder builder = new ConnetionBuilder(
				"http://www.wecreate.top/server.php");
		Map<String, String> params = new HashMap<>();
		params.put("action", "adduserinfo");
		builder.setRequestParams(params);
		//2. 通过builder来构建请求。
		DemoSnetConnection connection = new DemoSnetConnection(builder) {

			@Override
			public void onSuccess(String result) {
				// TODO 成功的值，此时在UI线程中

			}

			@Override
			public void onFail(String result) {
				// TODO 失败的值，此时在UI线程中

			}
		};
		//3. 正式发起请求。两种请求方式
		//connection.post();
		 connection.get();
	}
}
