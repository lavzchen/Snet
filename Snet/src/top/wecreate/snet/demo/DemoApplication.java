package top.wecreate.snet.demo;

import top.wecreate.snet.SnetConfig;
import top.wecreate.snet.SnetConfig.SnetConfigBuilder;
import android.app.Application;

public class DemoApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		// 这里初始化SnetConfig。
		// 别忘了在Manifest中声明Application的属性：
		// android:name="top.wecreate.snet.demo.DemoApplication"
		SnetConfig.init(new SnetConfigBuilder().setConnectionTimeout(2000));
	}

}
