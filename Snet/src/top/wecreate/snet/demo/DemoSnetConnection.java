package top.wecreate.snet.demo;

import java.util.List;
import java.util.Map;

import top.wecreate.snet.SnetBaseConnection;
import top.wecreate.snet.TestResponse;

//两个虚类型，分别用于正确的结果类型和出错时的结果类型。
public abstract class DemoSnetConnection extends
		SnetBaseConnection<String, String> {

	public DemoSnetConnection(ConnetionBuilder builder) {
		super(builder);
	}

	// 在线程中，对异常结果进行处理。
	@Override
	protected void processExceptionBackground(Exception e) {
		setFailResult(e.toString());
	}

	// 在线程中，对返回结果进行处理。
	@Override
	protected void processResponseBackground(int responseCode,
			Map<String, List<String>> responseHeaders, byte[] responseBody) {
		if (responseCode == 200) {
			// 网络返回值正确，处理后设置正确的值
			setSuccessResult(new String(responseBody));
		} else {
			// 不对，处理后设置错误的值。
			setFailResult("网络错误");
		}
	}

	// 使用本地离线数据测试异常情况，可以自定义异常情况
	@Override
	protected Exception fillTestException() {
		return new Exception("test exception");
	}

	// 使用本地离线数据测试正常返回。
	@Override
	protected TestResponse fillTestResponse() {
		TestResponse response = new TestResponse();
		response.setCode(200);
		response.setBody(new String("test response").getBytes());
		return response;
	}

}
