##Snet的初衷是什么？
    * 创建一个简单的请求框架.
    * 并且希望提供使用离线数据，来模拟请求的环境。
        特别是当服务器没有写好后台时，可以现在代码中使用测试数据来测试Android代码写的情况。

##Snet有哪些功能？
* 网络请求方法简单化
    *  仅支持GET，POST请求，android开发应该够了。
* 无需关注请求过程
    *  必须参数：请求地址
    *  可选参数：请求头，请求参数
* 返回结果就在UI线程中
    *  无需再转化和使用Handler
* 可设置离线数据模拟功能
    *  通过本地数据填充返回信息。模拟真实环境。 

##使用方法

###1.在Application中初始化Snet库
```java
// 这里初始化SnetConfig。
    	// 别忘了在Manifest中声明Application的属性：
		// android:name="top.wecreate.snet.demo.DemoApplication"
SnetConfig.init(new SnetConfigBuilder().setConnectionTimeout(2000));
```

###2.继承SnetBaseConnection，实现对结果的解析
```java
//两个虚类型，分别用于正确的结果类型和出错时的结果类型。
public abstract class DemoSnetConnection extends
    	SnetBaseConnection<String, String> {

	public DemoSnetConnection(ConnetionBuilder builder) {
		super(builder);
	}
//在线程中，对异常结果进行处理。
	@Override
	protected void processExceptionBackground(Exception e) {
		setFailResult(e.toString());
	}

//在线程中，对返回结果进行处理。
	@Override
	protected void processResponseBackground(int responseCode,
			Map<String, List<String>> responseHeaders, byte[] responseBody) {
		if (responseCode == 200) {
			// 网络返回值正确，处理后设置正确的值
			setSuccessResult(new String(responseBody));
		} else {
			// 不对，处理后设置错误的值。
			setFailResult("网络错误");
		}
	}

//使用本地离线数据测试异常情况，可以自定义异常情况
	@Override
	protected Exception fillTestException() {
		return new Exception("test exception");
	}
//使用本地离线数据测试正常返回。
	@Override
	protected TestResponse fillTestResponse() {
		TestResponse response = new TestResponse();
		response.setCode(200);
		response.setBody(new String("test response").getBytes());
		return response;
	}

}
```
###3. 在Activity中直接使用
```java
    //1. 构建请求所需的参数，比如url，参数之类的
		ConnetionBuilder builder = new ConnetionBuilder(
				"http://www.wecreate.top/server.php");
		Map<String, String> params = new HashMap<>();
		params.put("action", "adduserinfo");
		builder.setRequestParams(params);
		//2. 通过builder来构建请求。
		DemoSnetConnection connection = new DemoSnetConnection(builder) {

			@Override
			public void onSuccess(String result) {
				// TODO 成功的值，此时在UI线程中

			}

			@Override
			public void onFail(String result) {
				// TODO 失败的值，此时在UI线程中

			}
		};
		//3. 正式发起请求。两种请求方式
		//connection.post();
		 connection.get();
```

##有问题反馈
在使用中有任何问题，欢迎反馈给我；或者如果你有新的想法和建议，都可以用以下联系方式跟我交流

* 邮件: 814084764@qq.com



##关于作者
一个喜欢写代码，但总是写不好的农民工。